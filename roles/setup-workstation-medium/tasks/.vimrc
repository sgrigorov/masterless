set nu ruler et 
set ai si
set cuc
set sw=2 sts=2 ts=2
syntax enable
filetype plugin ident on

set ts=2 et sw=2 past number cuc

!!!!!! Shift+Ins - Paste Code in Vim

set all # shows teh current settings
set ts=2 # tabstop - Set tab width to 2 columns.
set sts=2 # softtabstop
set et # expandtab - Convert tabs to spaces
set sw=2 # shiftwidth - Set shift width to 2 spaces.
set past #
set nu # Add numbers to each line on the left-hand side.
set cuc # Highlight cursor line underneath the cursor vertically.
set cul # Highlight cursor line underneath the cursor horizontally.
set ai # auto-indent
set si # smartindent
set ic # ignore case during search.
filetype indent on # Enable indentation rules that are file-type specific.
syntax on # Turn syntax highlighting on
_____________________________________________

set tabstop=2 softtabstop=2 shiftwidth=2
set expandtab
set number ruler
set autoindent smartindent
syntax enable
filetype plugin indent on

:set expandtab
:set tabstop=4
:set shiftwidth=4
:retab

Indent all lines - gg=G
Show diff before saving the file - :w !diff % -
Delete text even when you are in Insert mode:
Ctrl + w: Delete previous word (equivalent to db in Normal mode)
Ctrl + h: Delete previous character
Ctrl + u: Delete all the previous characters until the start of line (equivalent to d0 in Normal mode)
Ctrl + k: Delete all the leading characters until the end of line (equivalent to d$ in Normal mode)

Ctrl+a - bash go to the start of the line
Ctrl+e - bash go to the end of the line
Ctrl+l - bash Clear the screen
Ctrl+r - recall bash history
Ctrl+w- cut the last word
Ctrl+u - (zsh cut the whole line)
Ctrl+y - paste




Ctrl+d - exit ssh session

Position the cursor at the beginning of the text you want to cut/copy.
Press v to begin character-based visual selection, or V to select whole lines, or Ctrl-v or Ctrl-q to select a block.
Move the cursor to the end of the text to be cut/copied. While selecting text, you can perform searches and other advanced movement.
Press d (delete) to cut, or y (yank) to copy.
Move the cursor to the desired paste location.
Press p to paste after the cursor, or P to paste before.

dG — This deletes everything from the location of your cursor right up to the end of the YAML file
. ==> (dot) to repeat last command

ctrl-t, ctrl-d  - indent current line forward, backwards 
                  (insert mode)
visual > or <   - indent block by sw (repeat with . )



source <(kubectl completion bash)
echo "source <(kubectl completion bash)" >> ~/.bashrc

echo 'alias k=kubectl' >>~/.bashrc
echo 'complete -o default -F __start_kubectl k' >>~/.bashrc

export do="--dry-run=client -o yaml"
# then we can run
k run pod1 --image=nginx $do

alias ka="kubectl apply -f"

truncate -s 0 ./logs/audit.log - empty the audit.log file

| Short name           | Full name                    |
| -------------------- | ---------------------------- |
|  csr                 |  certificatesigningrequests  |
|  cs                  |  componentstatuses           |
|  cm                  |  configmaps                  |
|  ds                  |  daemonsets                  |
|  deploy              |  deployments                 |
|  ep                  |  endpoints                   |
|  ev                  |  events                      |
|  hpa                 |  horizontalpodautoscalers    |
|  ing                 |  ingresses                   |
|  limits              |  limitranges                 |
|  ns                  |  namespaces                  |
|  no                  |  nodes                       |
|  pvc                 |  persistentvolumeclaims      |
|  pv                  |  persistentvolumes           |
|  po                  |  pods                        |
|  pdb                 |  poddisruptionbudgets        |
|  psp                 |  podsecuritypolicies         |
|  rs                  |  replicasets                 |
|  rc                  |  replicationcontrollers      |
|  quota               |  resourcequotas              |
|  sa                  |  serviceaccounts             |
|  svc                 |  services                    |

