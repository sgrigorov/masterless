Starting Nmap 7.93 ( https://nmap.org ) at 2023-05-02 14:15 MDT
Nmap scan report for portquiz.net (35.180.139.74)
Host is up (0.031s latency).
rDNS record for 35.180.139.74: ec2-35-180-139-74.eu-west-3.compute.amazonaws.com
Not shown: 995 filtered tcp ports (no-response)
PORT     STATE SERVICE
80/tcp   open  http
443/tcp  open  https
1720/tcp open  h323q931
2000/tcp open  cisco-sccp
5060/tcp open  sip

Nmap done: 1 IP address (1 host up) scanned in 19.69 seconds
