# masterless

run the following command:

Debian

wget -O - https://gitlab.com/sgrigorov/masterless/raw/master/install.sh | bash

RedHat

curl -s -L https://gitlab.com/sgrigorov/masterless/raw/master/install.sh | bash

FreeBSD

fetch https://gitlab.com/sgrigorov/masterless/raw/master/install.sh

sh install.sh

curl -s -L https://gitlab.com/sgrigorov/masterless/raw/master/install.sh | sh


# git clone

Ctrl+Shift+P -> Git: Clone

ssh://git@gitlab.com/sgrigorov/masterless

ssh://git@gitlab.com/sgrigorov/ipxe

# ssh keys

ssh-keygen -f ~/.ssh/id_rsa_gitlab

cat > ~/.ssh/config << EOF
Host gitlab.com
  User git
  Hostname gitlab.com
  IdentityFile ~/.ssh/id_rsa_gitlab
EOF

