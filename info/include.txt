Install ansible, git
ok - Create dir ctructure /usr/share/ansible
ok - Install libselinux-python
ok - Create a crontab job for ansible-pull

Flatpack and snap module

Gnome - gconf
Gnome - gsettings
Gnome Settings
Gnome Shell extensions - install and configure
    - extensions:
        Installed Extensions
            AlternateTab  by fmuellner ONOFF - Substitute Alt-Tab with a window based switcher that does not group by application.
            Applications Menu  by fmuellner ONOFF - Add a category-based menu for applications.
            Autohide Battery  by iskin ONOFF - Hide battery icon in top panel, if battery is fully charged and AC is connected.
            Battery Status  by milliburn ONOFF - A configurable lightweight battery charge indicator and autohider. Includes a few quirky display modes.
            Caffeine  by eon ONOFF - Disable the screensaver and auto suspend
            Clipboard Indicator  by Tudmotu ONOFF - Clipboard Manager extension for Gnome-Shell - Adds a clipboard indicator to the top panel, and caches clipboard history.
            Dash to Dock  by michele_g ONOFF - A dock for the Gnome Shell. This extension moves the dash out of the overview transforming it in a dock for an easier launching of applications and a faster switching between windows and desktops. Side and bottom placement options are available.
            Extensions  by petres ONOFF - Enable/disable easily gnome shell extensions from a menu in the top panel. Also allows to edit the settings of the extensions. Feedback welcome!
            HowDoI  by awamper ONOFF - Search http://stackexchange.com sites
            Launch new instance  by fmuellner ONOFF - Always launch a new instance when clicking in the dash or the application view.
            Media Player Indicator  by JasonLG1979 ONOFF - Control MPRIS Version 2 Capable Media Players.
            OpenWeather  by jens ONOFF - Weather extension to display weather information from https://openweathermap.org/ or https://darksky.net for almost all locations in the world.
            Places Status Indicator  by fmuellner ONOFF - System extension - Add a menu for quickly navigating places in the system. 
            Removable Drive Menu  by fmuellner ONOFF - A status menu for accessing and unmounting removable devices.
            Simple net speed  by bijignome ONOFF - Simply showing network speed. Left click to change modes:
            TaskBar  by zpydr - TaskBar displays icons of running applications on the top panel or alternatively on a new bottom panel. Activate, minimize or close tasks with a simple click.
            TopIcons  by ag ONOFF - Shows legacy tray icons on top
            Transparent Top Bar ONOFF - Add transparency to the topbar.
            User Themes  by fmuellner ONOFF - Load shell themes from user directory.
            Window List  by fmuellner ONOFF - Display a window list at the bottom of the screen.
            
            Installed Extensions
            Background Logo ONOFF - Overlay a tasteful logo on the background to enhance the user experience
            Hot-Corn-Dog  by euhiemf - This is a GNOME Shell extension where you can pick your own "hot corners" for toggling the overview, or for running custom applications. You can also change the "hot corner" for the message tray.
            
            gnome-shell-extension-background-logo-3.24.0-5.fc28.noarch
            
            gnome-shell-extension-user-theme-3.22.2-1.fc25.noarch
            gnome-shell-extension-common-3.22.2-1.fc25.noarch
            gnome-shell-extension-window-list-3.22.2-1.fc25.noarch
            gnome-shell-extension-places-menu-3.22.2-1.fc25.noarch
            gnome-shell-extension-drive-menu-3.22.2-1.fc25.noarch
            gnome-shell-extension-alternate-tab-3.22.2-1.fc25.noarch
            gnome-shell-extension-apps-menu-3.22.2-1.fc25.noarch
            gnome-shell-extension-launch-new-instance-3.22.2-1.fc25.noarch


DNF  -enable fastest mirror and DRPM

Time sync
Open Firewall
OK - Enable repositories - fedora-workstation-repositories, rpmfusion-nonfree-nvidia-driver, epel-release
Install netdata (similar to cockpit)
OK - Add lite - text
Modify Sources list
OK - Install and modify desktop backgrounds
OK - Install chrome
OK - install cockpit
OK - install gnometools - gnome-tweak-tool, dconf
OK - Install graphics - mypaint, gimp
install Java - java-openjdk, icedtea-web(fedora), icedtea-netx (ubuntu)
install media - vlc-core, kodi
OK - install nomachine
OK - install pdftools - master-pdf-editor
install play on linux
OK - install rpmfusion 
install and config ssh server
OK - install synergy
install/config time - timesyncd, chrony, system-timesynced
OK - Install tools - unzip, vim, vim-powerline, tilix, powerline, powerline-fonts, policycoreutils
OK - Install virtual - VirtualBox, vagrant, docker
yum install bash-completion bash-completion-extras
yum install epel-release
yum install bash-completion-extras
https://www.thegeekdiary.com/centos-rhel-7-how-to-disable-ipv6/

OK - VPN - mozila 51
OK -     forty client
OK -     openvpn
    
handle logs
Gnome - add minimize and maximize buttons 