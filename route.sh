#[manjaro ~]# ip route add 10.0.0.0/8 via 192.168.127.47
#[manjaro ~]# ip route del 0.0.0.0/128.0.0.0 via 192.168.127.47
#ip route del 128.0.0.0/128.0.0.0 via 192.168.127.47

tun0_ip_address=$(ip addr show dev tun0 | grep "inet " | xargs | cut -d " " -f 2)

echo $tun0_ip_address
echo ip route add 10.0.0.0/8 via $tun0_ip_address
echo ip route del 0.0.0.0/128.0.0.0 via $tun0_ip_address
echo ip route del 128.0.0.0/128.0.0.0 via $tun0_ip_address
ip route add 10.0.0.0/8 via $tun0_ip_address
ip route del 0.0.0.0/128.0.0.0 via $tun0_ip_address
ip route del 128.0.0.0/128.0.0.0 via $tun0_ip_address
############ before ############
#0.0.0.0/1 via 192.168.127.67 dev tun0 metric 1 
#default via 192.168.68.1 dev wlp4s0 proto dhcp src 192.168.68.118 metric 600 
#1.1.1.1 dev tun0 proto kernel scope link src 192.168.127.67 
#74.112.176.61 via 192.168.68.1 dev wlp4s0 src 192.168.68.118 metric 1 
#128.0.0.0/1 via 192.168.127.67 dev tun0 metric 1 
#192.168.68.1 via 192.168.68.118 dev wlp4s0 proto unspec metric 1 
############ after #############
#default via 192.168.68.1 dev wlp4s0 proto dhcp src 192.168.68.118 metric 600 
#1.1.1.1 dev tun0 proto kernel scope link src 192.168.127.67 
#10.0.0.0/8 via 192.168.127.67 dev tun0 
#74.112.176.61 via 192.168.68.1 dev wlp4s0 src 192.168.68.118 metric 1 
#192.168.68.0/24 dev wlp4s0 proto kernel scope link src 192.168.68.118 metric 600 
#192.168.68.1 via 192.168.68.118 dev wlp4s0 proto unspec metric 1 
