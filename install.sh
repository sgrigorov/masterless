if [ -f "/etc/os-release" ]; then
    os_id=`grep \^ID= /etc/os-release | sed "s/ID=//"| sed "s/\"//g"`
    os_ver_id=`grep \^VERSION_ID= /etc/os-release | sed "s/VERSION_ID=//"| sed "s/\"//g"`
    os_pretty_name=`grep \^PRETTY_NAME= /etc/os-release | sed "s/PRETTY_NAME=//"| sed "s/\"//g"`
    echo "OS:"$os_id"<"
    case $os_id in
        debian | ubuntu | zorin | pop | raspbian | elementary)
            echo "OS - " $os_pretty_name
            echo "Version ID " $os_ver_id
            apt-get update && apt-get install -y ansible git
        ;;
        centos | almalinux | rocky)
            echo "OS - " $os_pretty_name
            echo "Version ID " $os_ver_id
            case ${os_ver_id:0:1} in
                8)
                    echo $os_id $os_ver_id            
                    #echo -e "stan\tALL=(ALL)\tNOPASSWD:ALL" > /etc/sudoers.d/stan
                    #chmod 400 /etc/sudoers.d/stan
                    grep -q ^fastestmirror /etc/dnf/dnf.conf || echo fastestmirror=True >> /etc/dnf/dnf.conf
                    dnf -y install  epel-release
                    dnf -y update
                    dnf -y install  ansible git #qemu-guest-agent
                    # implemet if virtual for qemu-guest-agent
                    # if ( dmidecode -s systemproduct-name == "KVM/QEMU") install qemu-guest-agent
                    # if ( dmidecode -s systemproduct-name == "Virtual Box") install ???????
                    # Not needed !!! if ( dmidecode -s systemproduct-name == "VMware Virtual Platform") install open-vm-tools
                ;;
                7)
                    echo $os_id $os_ver_id     
                    #echo -e "stan\tALL=(ALL)\tNOPASSWD:ALL" > /etc/sudoers.d/stan
                    #chmod 400 /etc/sudoers.d/stan
                    yum -y install epel-release
                    yum -y update
                    yum -y install git ansible
                ;;
                *)
                    echo "Other CentOS"
                    #exit
                ;;
            esac
        ;;
        fedora)
            echo "OS - Fedora"
            echo "Version ID " $os_ver_id
            grep -q ^fastestmirror /etc/dnf/dnf.conf || echo fastestmirror=True >> /etc/dnf/dnf.conf
            dnf -y install  epel-release
            dnf -y update
            dnf -y install  ansible git #qemu-guest-agent
            # implemet if virtual for qemu-guest-agent
        ;;
        ol)
            echo "OS - Oracle Linux"
            echo "Version ID " $os_ver_id
            grep -q ^fastestmirror /etc/dnf/dnf.conf || echo fastestmirror=True >> /etc/dnf/dnf.conf
            dnf -y install  oracle-epel-release-el8
            dnf -y update
            dnf -y install  ansible git #qemu-guest-agent
            # implemet if virtual for qemu-guest-agent
        ;;
        manjaro | arch)
            echo "OS - " $os_pretty_name
            pacman -Sy
            pacman -S cronie ansible git --noconfirm --needed
        ;;
        freebsd)
            echo "OS - " $os_pretty_name
            pkg install -y pkg
            pkg update -y
            pkg install -y python38 py38-ansible git
        ;;
        *)
            echo "OS - Other"
            exit
        ;;
    esac
else
    exit
fi

ansible-pull -U https://gitlab.com/sgrigorov/masterless.git
